<table class="table table-striped">
    <thead>
    <tr>
        <th>Author</th>
        <th>Comment</th>
        @if($user->roles->first()->name == 'Admin')
            <th colspan="3">Action</th>
        @endif
    </tr>
    </thead>
    <tbody>
@forelse($comments as $comment)

@if($comment->approve =='1' && $user->roles->first()->name == 'User')
    <div class="display-comment">
        <tr><td><strong>{{ $comment->user->name }}</strong></td>

        <td><p>{{ $comment->body }}</p></td>
            @elseif($user->roles->first()->name == 'Admin')
                <div class="display-comment">
        <tr><td><strong>{{ $comment->user->name }}</strong></td>

            <td><p>{{ $comment->body }}</p></td>
     @endif
            @if($user->roles->first()->name == 'Admin')
        <td><a href="{{action('CommentsController@update', $comment['id'])}}" class="btn btn-warning">Approve</a></td>
            <td><form action=="{{action('CommentsController@edit', $comment['id'])}}" method="post">
                {{csrf_field()}}
                <button  class="btn btn-warning" type="submit">Reject</button>
        <td>
            <form action="{{action('CommentsController@destroy', $comment['id'])}}" method="post">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="DELETE">
                <button class="btn btn-danger" type="submit">Delete</button>
            </form>
        </td>
            @endif
        </tr>
       
    </div>
@empty
    <p>No Comments</p>
@endforelse
    </tbody>
</table>