<!-- index.blade.php -->
@extends('layouts.app')

@section('title')
  List of Products
  @endsection

@section('content')
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Products list Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
    <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
      <div class="row">
        <div class="col-md-5">

        </div>
        <div class="col-md-7 page-action text-right">
          @can('add_users')
            <a href="{{ action('ProductController@create') }}" class="btn btn-primary"> <i class="glyphicon glyphicon-plus-sign"></i> Add Products</a>
          @endcan
        </div>
      </div>
    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Price</th>
        <th>Description</th>
        @if($user->roles->first()->name == 'Admin')
        <th colspan="2">Action</th>
          @endif

      </tr>
    </thead>
    <tbody>
      @foreach($products as $product)
      <tr>
        <td>{{$product['id']}}</td>
        <td><a href="{{action('ProductController@show', $product['id'])}}">{{$product['name']}}</a></td>
        <td>{{$product['price']}}</td>
        <td>{{$product['description']}}</td>
        @if($user->roles->first()->name == 'Admin')
        <td><a href="{{action('ProductController@edit', $product['id'])}}" class="btn btn-warning">Edit</a></td>
        <td>
          <form action="{{action('ProductController@destroy', $product['id'])}}" method="post">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
            @endif
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
  </body>
</html>
@endsection