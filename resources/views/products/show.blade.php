<!-- show.blade.php -->
@extends('layouts.app')

@section('title')
    View a Product
    @endsection

@section('content')
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Product Detail Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
    <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
   <div>

        Name = {{$product['name']}} <br />
       Description = {{$product['description']}} <br />
        Price ={{$product['price']}}
        
  </div>
  <br />
        <hr />
     <h4>Display Comments</h4>
	 
 @include('products.commentsDisplay', ['comments' => $product->comments, 'product_id' => $product->id])
                    <hr />
                    <h4>Add comment</h4>
                    <form method="post" action="{{action('CommentsController@store', $id)}}">
                        @csrf
                        <div class="form-group">
                            <textarea class="form-control" name="body"></textarea>
                            <input type="hidden" name="product_id" value="{{$product['id']}}" />
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Add Comment" />
                        </div>
                    </form>

  </div>
  </body>
</html>
@endsection