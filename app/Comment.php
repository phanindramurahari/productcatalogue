<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;
use App\Presenters\DatePresenter;
class Comment extends Model
{
    //
	//use DatePresenter;
 
  // fields can be filled
  protected $fillable = ['body', 'user_id', 'product_id','approve'];
 
 
    public function user()
    {
        return $this->belongsTo(User::class);
    }
	  public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
