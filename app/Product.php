<?php

namespace App;
use App\Comment;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
	protected $fillable = ['name','price','description'];
public function comments()
{
  return $this->hasMany(Comment::class);
}
}
