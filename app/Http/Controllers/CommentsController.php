<?php
 
namespace App\Http\Controllers;
 
use App\Comment;
use App\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\CommentRequest;
 
class CommentsController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    public function index()
    {
        //
        $user = \Auth::user();
        $comments = Comment::all()->toArray();
        return view('comments.index', compact('comments','user'));
    }
   public function show()
    {
        //
        $user = \Auth::user();
       	$comments = Comment::find()->toArray();
        return view('comments.show', compact('comments','user'));
    }
    public function store(CommentRequest $request)
    {

        $post = Product::findOrFail($request->product_id);
		 $request->validate([
            'body'=>'required',
        ]);
        Comment::create([
            'body' => $request->body,
            'user_id' => Auth::id(),
            'product_id' => $post->id,
            'approve' => '0'
        ]);
        return back();
    }
    public function destroy($id)
    {
        //
        $comments = Comment::find($id);
        $comments->delete();
        return back();
    }

    public function update(CommentRequest $request, $id)
    {
        //
print 'www';
exit();

        $comments = Comment::find($id);

        $comments->body = $request->get('body');
        $comments->user_id = $request->get('user_id');
        $comments->product_id = $request->get('product_id');
        $comments->approve ='1';
        $comments->save();
        return back();
    }
    public function edit(CommentRequest $request, $id)
    {
        //
        print 'www';
        exit();

        $comments = Comment::find($id);

        $comments->body = $request->get('body');
        $comments->user_id = $request->get('user_id');
        $comments->product_id = $request->get('product_id');
        $comments->approve ='0';
        $comments->save();
        return back();
    }
}