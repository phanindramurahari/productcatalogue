<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('products');
})->middleware('auth');

Route::resource('products','ProductController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('products','ProductController');
Route::get('/products/{id}', 'ProductController@show')->name('products.show');
Route::resource('comments', 'CommentsController');
Route::group( ['middleware' => ['auth']], function() {
    Route::resource('users', 'UserController');
    Route::resource('roles', 'RoleController');
});