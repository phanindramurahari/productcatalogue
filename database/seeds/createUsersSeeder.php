<?php
use App\User;
use Illuminate\Database\Seeder;

class createUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'name' => 'guestuser',
            'email' => 'guestuser@gmail.com',
            'password' => Hash::make('guestuser'),
        ]);
    }
}
