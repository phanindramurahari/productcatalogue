<?php
use App\Product;
use Illuminate\Database\Seeder;

class createProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		  Product::create([
          'name' => 'product1',
          'price' => '1',
           'description' =>'Product 1 Description'
        ]);
        Product::create([
            'name' => 'product2',
            'price' => '2',
            'description' =>'Product 2 Description'
        ]);
        Product::create([
            'name' => 'product3',
            'price' => '3',
            'description' =>'Product 3 Description'
        ]);
    }
}
